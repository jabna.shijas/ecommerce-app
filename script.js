const apiUrl = 'https://fakestoreapi.com/products';
let cart = [];

// Function to fetch products and display them
const fetchProducts = async () => {
  try {
    const response = await fetch(apiUrl);
    const products = await response.json();
    displayProducts(products);
  } catch (error) {
    console.error('Error fetching products:', error);
  }
};

// Function to display products
const displayProducts = (products) => {
  const mainContent = document.getElementById('main-content');
  mainContent.innerHTML = ''; // Clear previous content

  products.forEach(product => {
    const productDiv = document.createElement('div');
    productDiv.className = 'product';
    productDiv.innerHTML = `
      <img src="${product.image}" alt="${product.title}">
      <h2>${product.title}</h2>
      <p>$${product.price}</p>
      <button onclick="viewProduct(${product.id})">View Details</button>
    `;
    mainContent.appendChild(productDiv);
  });
};

// Function to view product details
const viewProduct = async (id) => {
  try {
    const response = await fetch(`${apiUrl}/${id}`);
    const product = await response.json();
    displayProductDetails(product);
  } catch (error) {
    console.error('Error fetching product details:', error);
  }
};

// Function to display product details
const displayProductDetails = (product) => {
  const mainContent = document.getElementById('main-content');
  mainContent.innerHTML = `
    <div class="product-detail-wrapper">
      <div class="product-detail">
        <img src="${product.image}" alt="${product.title}">
        <div class="product-button">
          <button onclick="addToCart(${product.id})">Add to Cart</button>
          <button onclick="fetchProducts()">Back to Products</button>
        </div>
      </div>
      <div class="product-features">
        <h2>${product.title}</h2>
        <p>${product.description}</p>
        <p>$${product.price}</p>
      </div>
    </div>
  `;
};

// Function to add product to cart
const addToCart = (id) => {
  if (!cart.includes(id)) {
    cart.push(id);
  }
  document.getElementById('cart-count').textContent = cart.length;
  alert('Product added to cart!');
};

// Function to view cart
const viewCart = async () => {
  const mainContent = document.getElementById('main-content');
  mainContent.innerHTML = '<h2>Your Cart</h2>';
  
  if (cart.length === 0) {
    mainContent.innerHTML += '<p>Your cart is empty.</p>';
    return;
  }
  
  for (let id of cart) {
    try {
      const response = await fetch(`${apiUrl}/${id}`);
      const product = await response.json();
      const cartItemDiv = document.createElement('div');
      cartItemDiv.className = 'cart-item'; // Correct class for styling cart items
      cartItemDiv.innerHTML = `
        <img src="${product.image}" alt="${product.title}">
        <div class="cart-item-details">
          <h2>${product.title}</h2>
          <p>$${product.price}</p>
        </div>
      `;
      mainContent.appendChild(cartItemDiv);
    } catch (error) {
      console.error('Error fetching cart product:', error);
    }
  }
};

// Event listeners for navigation
document.getElementById('home-link').addEventListener('click', (e) => {
  e.preventDefault();
  fetchProducts();
});

document.getElementById('cart-link').addEventListener('click', (e) => {
  e.preventDefault();
  viewCart();
});

// Initial fetch of products
fetchProducts();

// Reload the page when the user returns to it
window.addEventListener('pageshow', (event) => {
  if (event.persisted || performance.navigation.type === 2) {
    window.location.reload();
  }
});
